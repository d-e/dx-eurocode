.. dx-punch documentation master file, created by
   sphinx-quickstart on Tue Dec 18 10:46:56 2018.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

Welcome to the documentation of ``dx-eurocode``
===============================================

A base package for structural design with `EN Eurocodes`_
---------------------------------------------------------

Features
^^^^^^^^

* Eurocode, Part 0 (|EC0|)

  * Safety factors

* Eurocode, Part 2 (|EC2|)

  * Materials
  * Recurrent formulas
  * Safety factors

.. Usage
.. -----
.. .. toctree::
..    chapters/usage

.. toctree::
   :hidden:
   :glob:

   _modules/modules

Indices and tables
------------------

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
