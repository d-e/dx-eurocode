.. Add links to use in source pages in the current folder

.. _EN Eurocodes: https://eurocodes.jrc.ec.europa.eu
.. _EN-1990: https://eurocodes.jrc.ec.europa.eu/showpage.php?id=130
.. _EN-1992: https://eurocodes.jrc.ec.europa.eu/showpage.php?id=132

.. |EC| replace:: `EC <EN Eurocodes_>`_
.. |EC0| replace:: `EC0 <EN-1990_>`_
.. |EC2| replace:: `EC2 <EN-1992_>`_
