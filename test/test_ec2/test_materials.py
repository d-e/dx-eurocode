from unittest import TestCase

from dx_eurocode.EC2.materials import Concrete, ReinforcementSteel
from dx_base.exceptions import UnrecognizedMaterialError
from dx_eurocode.EC2.formulas import nu66N
from dx_utilities.units import inverse_transform_value


class TestConcrete(TestCase):

    def setUp(self):
        self.C16 = Concrete(fck=16)
        self.C55 = Concrete(fck=55)

    def test_instantiation_raise(self):
        with self.assertRaises(UnrecognizedMaterialError):
            Concrete(fck=355)

    def test_fckcube(self):
        value = inverse_transform_value(self.C16.fck_cube)
        self.assertAlmostEqual(value, 20, places=0)

    def test_fcm(self):
        value = inverse_transform_value(self.C16.fcm)
        self.assertAlmostEqual(value, 24, places=0)

    def test_fctm(self):
        value = inverse_transform_value(self.C16.fctm)
        self.assertAlmostEqual(value, 1.9, places=1)

    def test_fctk5(self):
        value = inverse_transform_value(self.C16.fctk5)
        self.assertAlmostEqual(value, 1.3, places=1)

    def test_fctk95(self):
        value = inverse_transform_value(self.C16.fctk95)
        self.assertAlmostEqual(value, 2.5, places=1)

    def test_Ecm(self):
        value = inverse_transform_value(self.C16.Ecm, 'G')
        self.assertAlmostEqual(value, 29, places=0)

    def test_ec1(self):
        value = inverse_transform_value(self.C16.ec1, 'm')
        self.assertAlmostEqual(value, 1.9, places=1)

    def test_ecu1(self):
        value = inverse_transform_value(self.C16.ecu1, 'm')
        self.assertAlmostEqual(value, 3.5, places=1)
        value = inverse_transform_value(self.C55.ecu1, 'm')
        self.assertAlmostEqual(value, 3.2, places=1)

    def test_ec2(self):
        value = inverse_transform_value(self.C16.ec2, 'm')
        self.assertAlmostEqual(value, 2.0, places=1)
        value = inverse_transform_value(self.C55.ec2, 'm')
        self.assertAlmostEqual(value, 2.2, places=1)

    def test_ecu2(self):
        value = inverse_transform_value(self.C16.ecu2, 'm')
        self.assertAlmostEqual(value, 3.5, places=1)
        value = inverse_transform_value(self.C55.ecu2, 'm')
        self.assertAlmostEqual(value, 3.1, places=1)

    def test_n(self):
        self.assertAlmostEqual(self.C16.n, 2.0, places=1)
        self.assertAlmostEqual(self.C55.n, 1.75, places=1)

    def test_ec3(self):
        value = inverse_transform_value(self.C16.ec3, 'm')
        self.assertAlmostEqual(value, 1.75, places=1)
        value = inverse_transform_value(self.C55.ec3, 'm')
        self.assertAlmostEqual(value, 1.8, places=1)

    def test_ecu3(self):
        value = inverse_transform_value(self.C16.ecu3, 'm')
        self.assertAlmostEqual(value, 3.5, places=1)
        value = inverse_transform_value(self.C55.ecu3, 'm')
        self.assertAlmostEqual(value, 3.1, places=1)

    def test_safety_factor(self):
        factor = self.C16.safety_factor('persistent', 'ultimate')
        self.assertAlmostEqual(factor, 1.5)
        factor = self.C16.safety_factor('persistent', 'serviceability')
        self.assertAlmostEqual(factor, 1.)

    def test_CRdc(self):
        crdc = self.C16.CRdc('persistent', 'ultimate')
        self.assertAlmostEqual(crdc, 0.18 / 1.5)
        crdc = self.C16.CRdc('persistent', 'serviceability')
        self.assertAlmostEqual(crdc, 0.18 / 1.0)

    def test_fcd(self):
        fcd = self.C16.fcd('persistent', 'ultimate')
        self.assertAlmostEqual(fcd, 16e+06 / 1.5)

    def test_vrdmax(self):
        vrdmax = self.C16.vrdmax('persistent', 'ultimate')
        fck = inverse_transform_value(self.C16.fck)
        nu = nu66N(fck)
        fcd = self.C16.fcd('persistent', 'ultimate')
        self.assertAlmostEqual(vrdmax, 0.5*nu*fcd)


class TestReinforcementSteel(TestCase):

    def setUp(self):
        self.steel = ReinforcementSteel(fyk=400.)

    def test_fyk(self):
        self.assertAlmostEqual(self.steel.fyk, 400e+06)

    def test_ft(self):
        self.assertAlmostEqual(self.steel.ft, 1.05*400e+06)

    def test_eu(self):
        self.assertAlmostEqual(self.steel.eu, 2.5e-02)

    def test_fyd(self):
        self.assertAlmostEqual(self.steel.fyd(), 400e+06/1.15)

    def test_E(self):
        self.assertAlmostEqual(self.steel.E, 200e+09)
