from unittest import TestCase

from dx_eurocode.EC2.formulas import nu66N


class TestFormulas(TestCase):

    def test_nu66N(self):
        self.assertAlmostEqual(nu66N(250), 0.)
        self.assertAlmostEqual(nu66N(50), 0.48)
