# Issue title *Provide a short title of the issue*

## The problem

*Provide an extended description of the issue to be addressed. Keep in mind that the description should be clear and sufficient for someone who is new to the issue to understand the problem)*

## Deliverables

*Provide a list of deliverables (drawings, reports, moodboards, presentations etc)*

>> A report about whether we can build a bowling alley in the plot and why it is a good idea

## How do we get there?

*Provide a list of actions that need to be performed in order to address the issue and deliver the deliverables*

>> Ask the building regulators whether it is permitted and then check if there are other bowling alleys in a 10km radius



